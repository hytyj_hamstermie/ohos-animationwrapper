# AnimationWrapper

## 介绍
OpenHarmony ETS实现AnimateCSS动画库

现在只有两个动画，后续会陆续添加

> 动画效果参考：https://animate.style/

## 安装教程

> 请不要在DevEco IDE中添加第三方库

请按照以下步骤：

1、命令行进入项目entry目录，执行npm

```bash
npm install git+https://gitee.com/hytyj_hamstermie/ohos-animationwrapper.git
```



2、安装完成后打开IDE，查看entry/package.json可以看到已经安装依赖

![1](./figure/1.png)



3、若无法显示或者无法看到更新内容，请删除.preview文件夹后，重启previewer或者重启IDE。

## 使用说明

### 引入组件

```typescript
import { AnimationWrapper } from '@ohos/animationwrapper'
```

组件属性

- 动画名称（@Provide）：animationName: string

  - bounce

  - pulse 

- 动画持续时长：duration: number

- 动画执行次数：time: number

  至少执行1次，Infinity 执行无限次

- builder：组件构建器

  @Builder装饰器方法
  
- 动画执行完成：onAnimationComplete: ()=>void

  如果执行次数选择无限次则不会执行该方法

### 示例

```typescript
import { AnimationWrapper } from '@ohos/animationwrapper'

@Entry
@Component
struct Index {
  @Provide('animationName') name: string = ''

  @Builder AnimateText() {
    Column() {
      Text('Animate.css').fontSize(50).fontWeight(FontWeight.Bold).fontColor('#351c75')
    }
  }

  build() {
    Flex({
      alignItems: ItemAlign.Center,
      justifyContent: FlexAlign.Center,
      direction: FlexDirection.Column
    }) {

      Column() {
        AnimationWrapper({
          duration: 1000,
          time: 1,
          builder: this.AnimateText
        })
      }

      Button('执行动画 bounce').margin({ top: 50 }).onClick(async () => {
        this.name = 'bounce'
      })

      Button('执行动画 pulse').margin({ top: 20 }).onClick(async () => {
        this.name = 'pulse'
      })
    }
    .width('100%')
    .height('100%')
  }
}
```

